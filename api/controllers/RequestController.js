/**
 * RequestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    UserRequest(req,res){

        Request.create({
            userid : req.param('userid'),
            productid : req.param('productid'),
           
        }).then(Requestcreate=> {
            return res.ok(Requestcreate);
        }).catch(err => res.serverError(err));  

    },
    RequestList(req,res){

        Request.find().then(reqestList => res.ok(reqestList))
        .catch(err => res.notFound(err));
        console.log(res);
    },
    RequestUpdate(req,res){

        let collection = {};
        collection.status = req.param('status')
        // let requestid = ;
        
        Request.update({id:req.param('id')},collection).then(reqestUpdate => res.ok(reqestUpdate))
        .catch(err => res.notFound(err));
        console.log(res);
    },
    RequestCancel(req,res){
        Request.destroy({
            id : req.param('id')
        }).then( Request => res.ok(Request))
        .catch(err => res.serverError(err));
    },

};

