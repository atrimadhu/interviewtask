/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    add(req,res){
        Users.create({
            name : req.param('name'),
            email : req.param('email'),
            number : req.param('number'),
            password : req.param('password'),
            address : req.param('address'),
            age : req.param('age'),
        }).then(users=> {
            return res.ok(users);
        }).catch(err => res.serverError(err));  
    },

    list(req,res){
        Users.find().then(Users => res.ok(Users))
        .catch(err => res.notFound(err));
    },
    delete(req,res){
        Users.destroy({
            id : req.param('id')
        }).then( Users => res.ok(Users))
        .catch(err => res.serverError(err));
    },
    update(req,res){
        let collection = {};

        if (req.param('name')) {
            collection.name = req.param('name')
        }
        if (req.param('email')) {
            collection.email = req.param('email')
        }
        if (req.param('number')) {
            collection.number = req.param('number')
        }
        if (req.param('password')) {
            collection.password = req.param('password')
        }
        if (req.param('address')) {
            collection.address = req.param('address')
        }
        if (req.param('age')) {
            collection.age = req.param('age')
        }

        Users.update({
            id : req.param('id')
        },collection).then(Users => res.ok(Users))
        .catch(err => res.serverError(err));
    },
    login(req,res){
        email = req.param('email');
        password = req.param('password');
        Users.find({ email : email , password:password })
        .then(function (user){
            console.log(user);
            console.log(user.length);
            if (user.length == 0) { return res.json("Login Failed"); }
            else{ return res.json(user); }
            
          })
        .catch(err => res.notFound(err));
    }

};

