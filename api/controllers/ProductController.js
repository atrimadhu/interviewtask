/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    add(req,res){
        Product.create({
            productname : req.param('productname'),
            days : req.param('days'),
            price : req.param('price'),
            productqty : req.param('productqty'),
        }).then(product=> {
            return res.ok(product);
        }).catch(err => res.serverError(err));  
    },

    list(req,res){
        Product.find().then(product => res.ok(product))
        .catch(err => res.notFound(err));
    },
    delete(req,res){
        Product.destroy({
            id : req.param('id')
        }).then( product => res.ok(product))
        .catch(err => res.serverError(err));
    },
    update(req,res){
        let collection = {};

        if (req.param('productname')) {
            collection.productname = req.param('productname')
        }
        if (req.param('days')) {
            collection.days = req.param('days')
        }
        if (req.param('price')) {
            collection.price = req.param('price')
        }
        if (req.param('productqty')) {
            collection.productqty = req.param('productqty')
        }
        Product.update({
            id : req.param('id')
        },collection).then(product => res.ok(product))
        .catch(err => res.serverError(err));
    },
    
    

};

