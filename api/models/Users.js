


module.exports = {
  
    attributes :{
        name :{
            type:'string',
            required: true,
      
        },
        email:{
            type:'string',
            required: true,
            unique:true
        },
        number:{
            type:'string',
            required: true,
        },
        password:{
            type:'string',
            required: true,
        },
        address:{
            type:'string',
            required: true,
        },
        age:{
            type:'string',
            required: true,
        },


        
    },
    
};